package cc.sunni.sell.controller;

import cc.sunni.sell.aspect.SellerAuthorizeAspect;
import cc.sunni.sell.constant.CookieConstant;
import cc.sunni.sell.constant.RedisConstant;
import cc.sunni.sell.dto.OrderDto;
import cc.sunni.sell.entity.SellerInfo;
import cc.sunni.sell.enums.ResultEnum;
import cc.sunni.sell.exception.RRException;
import cc.sunni.sell.service.OrderMasterService;
import cc.sunni.sell.service.SellerInfoService;
import cc.sunni.sell.utils.CookieUtil;
import cc.sunni.sell.utils.PageUtils;
import cc.sunni.sell.utils.Query;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;

/**
 * @author jl
 * @since 2021/1/21 19:22
 */
@Controller
@RequestMapping("/seller/order")
@Slf4j
public class SellerOrderController {
    @Autowired
    private OrderMasterService orderService;

    /**
     * 订单列表
     */
    @GetMapping("/list")
    public ModelAndView list(@RequestParam(value = "page", defaultValue = "1") Integer page,
                             @RequestParam(value = "size", defaultValue = "10") Integer size,
                             Map<String, Object> map) {
        Map<String, Object> params = new HashMap<>();
        params.put(Query.PAGE_NO, page);
        params.put(Query.SIZE, size);
        PageUtils pageUtils = orderService.queryPage(params);
        map.put("orderDTOPage", pageUtils);
        map.put("currentPage", page);
        map.put("size", size);
        map.put("sellerId", SellerAuthorizeAspect.getUserInfo().getId());
        return new ModelAndView("order/list", map);
    }


    /**
     * 取消订单
     */
    @GetMapping("/cancel")
    public ModelAndView cancel(@RequestParam("orderId") String orderId, Map<String, Object> map) {
        try {
            OrderDto orderDTO = orderService.findOne(orderId);
            orderService.cancel(orderDTO);
        } catch (RRException e) {
            log.error("【卖家端取消订单】发生异常{}", e.getMessage());
            map.put("msg", e.getMessage());
            map.put("url", "/sell/seller/order/list");
            return new ModelAndView("common/error", map);
        }

        map.put("msg", ResultEnum.ORDER_CANCEL_SUCCESS.getMessage());
        map.put("url", "/sell/seller/order/list");
        return new ModelAndView("common/success");
    }

    /**
     * 订单详情
     */
    @GetMapping("/detail")
    public ModelAndView detail(@RequestParam("orderId") String orderId, Map<String, Object> map) {
        OrderDto orderDTO;
        try {
            orderDTO = orderService.findOne(orderId);
        } catch (RRException e) {
            log.error("【卖家端查询订单详情】发生异常{}", e.getMessage());
            map.put("msg", e.getMessage());
            map.put("url", "/sell/seller/order/list");
            return new ModelAndView("common/error", map);
        }

        map.put("orderDTO", orderDTO);
        return new ModelAndView("order/detail", map);
    }

    /**
     * 完结订单
     */
    @GetMapping("/finish")
    public ModelAndView finished(@RequestParam("orderId") String orderId, Map<String, Object> map) {
        try {
            OrderDto orderDTO = orderService.findOne(orderId);
            orderService.finish(orderDTO);
        } catch (RRException e) {
            log.error("【卖家端完结订单】发生异常{}", e.getMessage());
            map.put("msg", e.getMessage());
            map.put("url", "/sell/seller/order/list");
            return new ModelAndView("common/error", map);
        }

        map.put("msg", ResultEnum.ORDER_FINISH_SUCCESS.getMessage());
        map.put("url", "/sell/seller/order/list");
        return new ModelAndView("common/success");
    }
}
