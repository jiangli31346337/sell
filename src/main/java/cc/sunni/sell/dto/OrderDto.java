package cc.sunni.sell.dto;

import cc.sunni.sell.entity.OrderDetail;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * @author jl
 * @since 2021/1/18 19:03
 */
@Data
public class OrderDto {
    private String orderId;
    /**
     * 买家名字
     */
    private String buyerName;
    /**
     * 买家电话
     */
    private String buyerPhone;
    /**
     * 买家地址
     */
    private String buyerAddress;
    /**
     * 买家微信openid
     */
    private String buyerOpenid;
    /**
     * 订单总金额
     */
    private BigDecimal orderAmount;
    /**
     * 订单状态, 默认为新下单
     */
    private Integer orderStatus;
    private String orderStatusCN;
    /**
     * 支付状态, 默认未支付
     */
    private Integer payStatus;
    private String payStatusCN;
    /**
     * 创建时间
     */
    private Date createTime;
    /**
     * 修改时间
     */
    private Date updateTime;

    private List<OrderDetail> orderDetailList;

    private String sellerId;
}
