package cc.sunni.sell.utils;

import cc.sunni.sell.enums.ResultEnum;

import java.util.HashMap;
import java.util.Map;

/**
 * 返回数据
 */
public class R extends HashMap<String, Object> {
    private static final long serialVersionUID = 1L;

    public R() {
        put("code", 0);
        put("success", true);
        put("msg", "操作成功");
    }

    public static R ok() {
        return new R();
    }

    public static R ok(Object value) {
        R r = new R();
        r.put("data", value);
        return r;
    }

    public static R error() {
        return error("系统繁忙,请稍候重试");
    }

    public static R error(String msg) {
        R r = new R();
        r.put("code", 500);
        r.put("success", false);
        r.put("msg", msg);
        return r;
    }

    public static R error(int code, String msg) {
        R r = new R();
        r.put("code", code);
        r.put("msg", msg);
        r.put("success", false);
        return r;
    }

    public static R map(Map<String, Object> map) {
        R r = new R();
        r.putAll(map);
        return r;
    }

    public static R em(ResultEnum resultEnum){
        R r = new R();
        r.put("code", resultEnum.getCode());
        r.put("msg", resultEnum.getMessage());
        return r;
    }

}
