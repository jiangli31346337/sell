package cc.sunni.sell.utils;

import com.baomidou.mybatisplus.core.metadata.IPage;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * 分页工具类
 */
@Data
public class PageUtils implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 当前页数
     */
    private int pageNo;
    /**
     * 每页记录数
     */
    private int size;
    /**
     * 总记录数
     */
    private long total;

    /**
     * 列表数据
     */
    private List<?> list;
    /**
     * 总页数
     */
    private int totalPages;

    /**
     * 分页
     *
     * @param list       列表数据
     * @param totalPages 总记录数
     * @param size       每页记录数
     * @param pageNo     当前页数
     */
    public PageUtils(List<?> list, int totalPages, int size, int pageNo) {
        this.list = list;
        this.total = totalPages;
        this.size = size;
        this.pageNo = pageNo;
        this.totalPages = (int) Math.ceil((double) totalPages / size);
    }

    /**
     * 分页
     */
    public PageUtils(IPage<?> pageNo) {
        this.list = pageNo.getRecords();
        this.total = (int) pageNo.getTotal();
        this.size = (int) pageNo.getSize();
        this.pageNo = (int) pageNo.getCurrent();
        this.totalPages = (int) pageNo.getPages();
    }


}
