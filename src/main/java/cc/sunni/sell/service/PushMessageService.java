package cc.sunni.sell.service;

import cc.sunni.sell.dto.OrderDto;

/**
 * 推送消息
 * @author jl
 * @since 2021/1/24 13:59
 */
public interface PushMessageService {

    void orderStatus(OrderDto orderDTO);
}
