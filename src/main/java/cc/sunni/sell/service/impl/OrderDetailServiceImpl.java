package cc.sunni.sell.service.impl;

import cc.sunni.sell.utils.PageUtils;
import cc.sunni.sell.utils.Query;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cc.sunni.sell.dao.OrderDetailDao;
import cc.sunni.sell.entity.OrderDetail;
import cc.sunni.sell.service.OrderDetailService;
import org.springframework.stereotype.Service;
import java.util.Map;

@Service("orderDetailService")
public class OrderDetailServiceImpl extends ServiceImpl<OrderDetailDao, OrderDetail> implements OrderDetailService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<OrderDetail> page = this.page(
                new Query<OrderDetail>().getPage(params),
                // 拼接查询条件
                new LambdaQueryWrapper<OrderDetail>()
        );

        return new PageUtils(page);
    }

}