package cc.sunni.sell.service;

import cc.sunni.sell.utils.PageUtils;
import com.baomidou.mybatisplus.extension.service.IService;
import cc.sunni.sell.entity.OrderDetail;
import java.util.Map;

/**
 * 订单详情
 *
 * @author jl
 * @since 2021-01-17 19:27:13
 */
public interface OrderDetailService extends IService<OrderDetail> {

    PageUtils queryPage(Map<String, Object> params);
}

