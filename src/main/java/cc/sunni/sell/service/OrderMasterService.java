package cc.sunni.sell.service;

import cc.sunni.sell.dto.OrderDto;
import cc.sunni.sell.utils.PageUtils;
import com.baomidou.mybatisplus.extension.service.IService;
import cc.sunni.sell.entity.OrderMaster;
import java.util.Map;

/**
 * 订单表
 *
 * @author jl
 * @since 2021-01-17 19:27:13
 */
public interface OrderMasterService extends IService<OrderMaster> {

    /**
     * 创建订单
     */
    String create(OrderDto orderDto);

    /**
     * 查询单个订单
     */
    OrderDto findOne(String orderId);

    /**
     * 查询订单列表
     */
    PageUtils queryPage(Map<String, Object> params);

    /**
     * 取消订单
     */
    void cancel(OrderDto orderDto);

    /**
     * 完结订单
     */
    void finish(OrderDto orderDto);

    /**
     * 修改订单支付状态
     */
    void pay(OrderDto orderDto);
}

