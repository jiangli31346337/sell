package cc.sunni.sell.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.math.BigDecimal;
import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 订单详情
 * 
 * @author jl
 * @since 2021-01-17 19:27:13
 */
@Data
@TableName("order_detail")
public class OrderDetail implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 主键ID
	 */
    @TableId(type = IdType.ASSIGN_ID)
    private String detailId;
	/**
	 * 订单id
	 */
    private String orderId;
	/**
	 * 商品id
	 */
    private String productId;
	/**
	 * 商品名称
	 */
    private String productName;
	/**
	 * 当前价格,单位分
	 */
    private BigDecimal productPrice;
	/**
	 * 数量
	 */
    private Integer productQuantity;
	/**
	 * 小图
	 */
    private String productIcon;
	/**
	 * 创建时间
	 */
    private Date createTime;
	/**
	 * 修改时间
	 */
    private Date updateTime;

}
