package cc.sunni.sell.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @author jl
 * @since 2021/1/17 21:28
 */
public class SellEnums {

    @Getter
    @AllArgsConstructor
    public enum ProductStatusEnum {
        /**
         * 商品状态
         */
        UP(0, "上架"),
        DOWN(1, "下架");
        private final Integer code;
        private final String message;

    }

    @AllArgsConstructor
    @Getter
    public enum OrderStatusEnum {
        /**
         * 订单状态
         */
        NEW(0, "新订单"),
        FINISHED(1, "完结"),
        CANCEL(2, "已取消"),
        ;
        private final Integer code;
        private final String message;

    }

    @AllArgsConstructor
    @Getter
    public enum PayStatusEnum {
        /**
         * 支付状态
         */
        WAIT(0, "等待支付"),
        SUCCESS(1, "支付成功"),
        ;
        private final Integer code;
        private final String message;
    }

}
