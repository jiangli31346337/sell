package cc.sunni.sell.validator;

import cc.sunni.sell.utils.RegexUtils;
import org.apache.commons.lang3.StringUtils;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

/**
 * @author jl
 * @since 2021/1/24 10:55
 */
public class IsIdCardValidator implements ConstraintValidator<IsIdCard, String> {

    private boolean required = false;

    @Override
    public void initialize(IsIdCard constraintAnnotation) {
        required = constraintAnnotation.required();
    }

    @Override
    public boolean isValid(String value, ConstraintValidatorContext constraintValidatorContext) {
        if(required) {
            return RegexUtils.checkMobile(value);
        }else {
            return StringUtils.isEmpty(value) || RegexUtils.checkIdCard(value);

        }
    }
}
