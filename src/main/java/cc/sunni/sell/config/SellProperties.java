package cc.sunni.sell.config;

import lombok.Data;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * @author jl
 * @since 2021/1/19 22:05
 */
@Data
@Component
@ConfigurationProperties(prefix = "sell")
public class SellProperties implements InitializingBean {

    private String domain;
    private String sell;

    public static String DOMAIN;
    public static String SELL;

    @Override
    public void afterPropertiesSet() {
        DOMAIN = domain;
        SELL = sell;
    }
}
