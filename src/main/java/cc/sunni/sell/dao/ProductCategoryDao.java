package cc.sunni.sell.dao;

import cc.sunni.sell.entity.ProductCategory;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 商品类目
 * 
 * @author jl
 * @since 2021-01-17 19:27:13
 */
@Mapper
public interface ProductCategoryDao extends BaseMapper<ProductCategory> {
	
}
